#include <QApplication>
#include "controlWindow.h"
#include "regelaar.h"

ControlWindow::ControlWindow(QWidget *parent): QMainWindow(parent) {
    QDateTime now {QDateTime::currentDateTime()};
    QString nowString {now.toString("yyyy-MM-dd-hh-mm-ss")};
    QString ofileName {"Resistor-"};
    ofileName += nowString + ".txt";
    ofile.open(ofileName.toLocal8Bit().data());
    if (ofile) {
        ofile << "tijd(seconden) setpoint(gradenC) temperatuur(gradenC) PWM(procent)\n";
    }

    seriesY = new QLineSeries();
    seriesY->setColor(QColor(204, 0, 51));
    seriesY->setName("Temperatuur (°C)");
    seriesX = new QLineSeries();
    seriesX->setColor(QColor("blue"));
    seriesX->setName("PWM (%)");
    seriesS = new QLineSeries();
    seriesS->setColor(QColor("green"));
    seriesS->setName("Setpoint (°C)");

    chart = new QChart();
    chart->addSeries(seriesS);
    chart->addSeries(seriesX);
    chart->addSeries(seriesY);
    chart->createDefaultAxes();
    QValueAxis* xas {dynamic_cast<QValueAxis*>(chart->axes(Qt::Horizontal).first())};
    if (xas) {
        xas->setTickCount(11);
        xas->setLabelFormat("%d");
        xas->setRange(0, maxSamples/10);
        xas->setTitleText("Time (s)");
        QFont font {xas->titleFont()};
        font.setWeight(QFont::Normal);
        xas->setTitleFont(font);

    }
    QValueAxis* yas {dynamic_cast<QValueAxis*>(chart->axes(Qt::Vertical).first())};
    if (yas) {
        yas->setTickCount(11);
        yas->setLabelFormat("%d");
        yas->setRange(0, 100);
    }

    chart->legend()->setAlignment(Qt::AlignBottom);
    chart->setTitle("Opwarmen van een weerstand");

    chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    timer = new QTimer(nullptr);
    timer->callOnTimeout(this, &ControlWindow::tick);
    timer->start(100);

    layoutV = new QVBoxLayout;
    layoutH = new QHBoxLayout;
    startStop = new QPushButton("&Start", this);
    connect(startStop, &QPushButton::clicked, this, &ControlWindow::startStopClicked);
    layoutH->addWidget(startStop);
    label1 = new QLabel("Setpoint:");
    label1->setFixedWidth(50);
    layoutH->addWidget(label1);
    spinbox = new QSpinBox(this);
    spinbox->setMinimum(20);
    spinbox->setMaximum(100);
    spinbox->setValue(setpoint);
    spinbox->setFixedWidth(50);
    connect(spinbox, QOverload<int>::of(&QSpinBox::valueChanged), this, &ControlWindow::setpointChanged);
    layoutH->addWidget(spinbox);
    layoutH->insertSpacing(-1, 7);
    line = new QFrame();
    line->setFrameShape(QFrame::VLine);
    line->setFrameShadow(QFrame::Sunken);
    layoutH->addWidget(line);
    checkboxRuis = new QCheckBox("NOISE", this);
    connect(checkboxRuis, &QCheckBox::stateChanged, this, &ControlWindow::ruisChanged);
    layoutH->addWidget(checkboxRuis);
    checkboxRuis->setCheckState(Qt::Checked);
    addNoise = true;
    checkboxBediening = new QCheckBox("ManualPWM", this);
    connect(checkboxBediening, &QCheckBox::stateChanged, this, &ControlWindow::bedieningChanged);
    layoutH->addWidget(checkboxBediening);
    label2 = new QLabel("PWM:");
    label2->setEnabled(false);
    layoutH->addWidget(label2);
    slider = new QSlider(Qt::Horizontal, this);
    slider->setRange(0, 1000);
    slider->setTickInterval(100);
    slider->setTickPosition(QSlider::TicksBelow);
    checkboxBediening->setFixedHeight(slider->height());
    slider->setEnabled(false);
    connect(slider, &QSlider::valueChanged, this, &ControlWindow::inputChanged);
    layoutH->addWidget(slider);
    layoutV->addLayout(layoutH);
    layoutV->addWidget(chartView);

    widget = new QWidget(this);

    setCentralWidget(widget);
    widget->setLayout(layoutV);
    resize(1000, 750);
}

ControlWindow::~ControlWindow() {
    delete widget;
    delete label1;
    delete label2;
    delete line;
    delete checkboxBediening;
    delete checkboxRuis;
    delete startStop;
    delete slider;
    delete spinbox;
    delete layoutV;
    delete layoutH;
    delete timer;
    delete chartView;
    delete chart;
    delete seriesY;
    delete seriesX;
    if (ofile)
        ofile.close();
}

void ControlWindow::setX(double value) {
    x = value;
}

double ControlWindow::getY() {
    return y;
}

int ControlWindow::getSetpoint() {
    return setpoint;
}

void ControlWindow::setTickInterval(int value) {
    tickInterval = value;
}

void ControlWindow::tick()
{
    if (running) {
        double systeem(double x);
        static int n {0};
        if (seriesX->count() == maxSamples + 1) {
            seriesX->remove(0);
            seriesY->remove(0);
            seriesS->remove(0);
            static QValueAxis* xas {dynamic_cast<QValueAxis*>(chart->axes(Qt::Horizontal).first())};
            xas->setLabelFormat("%.1f");
            xas->setRange((n-maxSamples)/10.0, n/10.0);
        }
        y = systeem(x);
        if (!slider->isEnabled() && n % tickInterval == 0) {
            regel();
        }
        seriesS->append(n/10.0, setpoint);
        seriesX->append(n/10.0, x * 100);
        seriesY->append(n/10.0, y);
        if (ofile) {
            ofile << std::fixed << std::setw(6) << std::setprecision(1) << n/10.0 << std::setw(4) << setpoint << std::setw(7) << std::setprecision(2) << y << std::setw(7) << std::setprecision(2) << x * 100 << '\n';
        }
        n++;
    }
}

void ControlWindow::startStopClicked() {
    if (running) {
        running = false;
        startStop->setText("&Start");
    }
    else {
        running = true;
        startStop->setText("&Stop");
    }
}

void ControlWindow::inputChanged(int value) {
    x = value/1000.0;
}

void ControlWindow::bedieningChanged(int value) {
    label2->setEnabled(value);
    slider->setEnabled(value);
    slider->setValue(x * 1000);
}

void ControlWindow::ruisChanged(int value) {
    addNoise = value;
}

void ControlWindow::setpointChanged(int value) {
    setpoint = value;
}

static ControlWindow* window;

// C interface:
int16_t readADC(void) {
    return static_cast<int16_t>(window->getY() * 4);
}

void setPWM(uint16_t dutyCycle) {
    window->setX(dutyCycle/1000.0);
}

uint16_t getSetpoint(void) {
    return window->getSetpoint();
}

void setIntervalTimer(uint8_t intervalTime) {
    if (intervalTime < 1) {
        intervalTime = 1;
    }
    else if (intervalTime > 100) {
        intervalTime = 100;
    }
    window->setTickInterval(intervalTime);
}

// call-back from systeem:
void showMessage(QString msg) {
    window->setWindowTitle(window->windowTitle() + ": " + msg);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    window = new ControlWindow;
    window->setWindowTitle("Temperatuurcontrol");
    window->show();
    init();
    return a.exec();
}
