#include "regelaar.h"

void init(void) {
    setIntervalTimer(5);
}
double q0 = 0,q1= 0 ,q2 = 0;
double td = 0,ts= 0.5 ,ti = 0, kc =0;
/*this is a open loop method approach
where the systeem constant or determine by an 1ste orde systeem analyse
The step response of a pure first order system in percent of the change of the output signal
is equal to (1 − e^(-t/tsys)·100%)
If we enter t = τsys this gives about 63.2% and if we add t =1/3*tsys give us a value of 28.3%
further more the systeem constant where determine by  setting a stap of 100% which = PWM(1000)
*/
double ksys =0.67*100,tsys= 39.75 ,teta = 2.25;
double output[3] = {0};
double error[3] = {0};
double setpoint = 0;
double input = 0;
double pid = 0;
int k =2; // help variabel

void regel(void)
{
    //// PID parameters Forward rectangular method & Ziegler-Nichols tuning
    ///  we change the veloctiy algoritme into a distrecte time using numerical FRM & 2 point formulaa
    kc = ((1.2 * tsys) / (ksys * teta));
    ti = 2 * teta;
    td = 0.5 * teta;
    q0 = kc * (1 + (td/ts));
    q1 = kc * (-1 + (ts/ti) - (2 * td /  ts));
    q2 = kc * ((td / ts));
    //
    setpoint = (double)(getSetpoint());
    input = (readADC()/4); // change the adc value to real degree celsius
    //shiting the delay output values
    output[k-2] = output[k-1];
    output[k-1] = output[k];
    output[k] = input; // input is not the input of the PID, the error term is
    // calculating de error term is setpoint-output value for each delay term
    error[0] = (setpoint - output[k]);
    error[1] = (setpoint - output[k-1]);
    error[2] = (setpoint - output[k-2]);
    // E.q of PID tpye A using veloctiy algoritme
    pid = pid + (q0 * error[0]) + (q1 * error[1]) + (q2 * error[2]);
    // saturate the pid output from 0-100
    if (pid < 0){
        setPWM(0);
    }
    else if(pid > 100){
        setPWM(1000);
    }
    else{
       setPWM(pid*10); // pwm is a form of 0-1000, thus a x10 is needed
    }

}

/*4.5 open loop measure values
 * O = 2.1s via doc
 * Teind = 87C Tstart = 20C  DT=87-20=67C
 * t2 DT*0623=42.34+20  t1=DT*.283 = 18.96+20
 * t1 via doc = 15.5s t2 = 42sec
 * tsys = 3/2(42-15.5) = 39.75 , O= 42-39.75=2.25
 * Ksys = 67/100 = 0.67
*/
