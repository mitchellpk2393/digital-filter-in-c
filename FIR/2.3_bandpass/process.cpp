#include <stdint.h>
#include <process.h>
#include "bandpass_q_15.h"
#include <math.h>

// this code is mainly used for Qt project with soundscope and VB cable
// but the snip code in process left can also be used for micro controller where
// there is no floating point unit

// the goal of this program is a bandpass FIR filter f1=6Khz f2= 9Khz
// you can use floating point number to by having to correct coefficient and changing all type to double
static const int N = BL;
int32_t buffer[N] = {0};

int k = 0;
int i = 0;
int j = 0;
// taking a sample of soundcard scope and then return it to soundscard scope bascially
int16_t process_left_sample ( int16_t sample )
 {
    //sample code which must be fine tuned and can be implemented in a Microcontroller
    buffer[j] = sample; 
    int32_t output = 0;
    j = i;
    for(k = 0; k <= N-1;k++)
    {
        output = output +(int32_t)(B[k]*buffer[j]); // a cheap circulair buffer
        j--;
        if(j <0)
        {
            j = (N-1);
        }
    }
    k=0;
    i = (i+1) % N;
   return output>>15; // shifting right is like divide by 2^15 to convert back to
 }

int16_t process_right_sample ( int16_t sample )
  {
    return sample; // not in use
  }
