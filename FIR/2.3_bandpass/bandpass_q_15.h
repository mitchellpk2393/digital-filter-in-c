// bandstop FIR filter 22orde, where by the coefficients are in Q0.15 format
// exported out a matlab filterdesginer tool
#define BL 23
const int16_t B[23] = {
     1522,   -991,   -326,      0,    426,   1707,  -3517,      0,   5909,
    -5205,  -3053,   8192,  -3053,  -5205,   5909,      0,  -3517,   1707,
      426,      0,   -326,   -991,   1522
};
