#ifndef PROCESS_H
#define PROCESS_H
#include <stdint.h>

// Define the sample_rate here:
const size_t sample_rate = 8000;

// Functions to process audio samples
int16_t process_left_sample(int16_t sample);
int16_t process_right_sample(int16_t sample);

#endif // PROCESS_H
