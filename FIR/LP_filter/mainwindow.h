#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAudioInput>
#include <QAudioOutput>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_playButton_clicked();
    void on_stopButton_clicked();
    void on_checkBox_stateChanged(int arg1);
    void readMore();

    void on_comboBoxInput_activated(int index);

    void on_comboBoxOutput_activated(int index);

private:
    void initializeAudioInput(const QAudioDeviceInfo& inputDeviceInfo);
    void initializeAudioOutput(const QAudioDeviceInfo& outputDeviceInfo);
    Ui::MainWindow *ui;
    QAudioFormat format;
    QAudioInput* audioInput;
    QAudioOutput* audioOutput;
    QIODevice* input;
    QIODevice* output;
    QByteArray buffer;
    bool process_samples;
    bool playing;
};

#endif // MAINWINDOW_H
