#include <stdint.h>
#include <process.h>
#include "fdacoefs_ldf_1K_fp.h"
// this code is mainly used for Qt project with soundscope and VB cable
// but the snip code in process left can also be used for micro controller with floating point number
// the goal of this program is a lowpass FIR filter fc=1Khz
static const int N = BL;
static double buffer[BL] = {0};
static int k = 0;
static int i = 0;
static double output = 0;
//

// taking a sample of soundcard scope and then return it to soundscard scope bascially
int16_t process_left_sample ( int16_t sample )
 {
    //sample code which must be fine tuned and can be implemented in a Microcontroller
    buffer[0] = sample;
    output = 0;
    while(k <= N-1)
    {
        output = output +(B[k]*buffer[k]);
        k++;
    }
    k=0;
    i = N-1;
    while(i >= 1)
    {
        buffer[i] = buffer[i-1];// moving the sample value to the right of the array
        i--;
    }
   return output;
 }

int16_t process_right_sample ( int16_t sample )
  {
    return sample;
  }
