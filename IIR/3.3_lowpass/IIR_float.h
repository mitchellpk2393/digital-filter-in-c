// export out of MATLAB filterdesigner
// IIR lowpassfilter Fc=1Khz
#define NL  3
const double B[3] = {
    0.09763107449,    0.195262149,  0.09763107449
};
#define DL 3
const double A[3] = {
                1,  -0.9428090453,   0.3333333433
};
