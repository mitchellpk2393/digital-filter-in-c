/*
 * Filter Coefficients (C Source) generated by the Filter Design and Analysis Tool
 * Generated by MATLAB(R) 9.8 and DSP System Toolbox 9.10.
 * Generated on: 02-Jun-2020 13:51:20
 */

/*
 * Discrete-Time IIR Filter (real)
 * -------------------------------
 * Filter Structure    : Direct-Form I
 * Numerator Length    : 3
 * Denominator Length  : 3
 * Stable              : Yes
 * Linear Phase        : No
 * Arithmetic          : fixed
 * Numerator           : s16,17 -> [-2.500000e-01 2.500000e-01)
 * Denominator         : s16,14 -> [-2 2)
 * Input               : s16,15 -> [-1 1)
 * Output              : s16,15 -> [-1 1)
 * Numerator Prod      : s32,32 -> [-5.000000e-01 5.000000e-01)
 * Denominator Prod    : s32,29 -> [-4 4)
 * Numerator Accum     : s34,32 -> [-2 2)
 * Denominator Accum   : s34,29 -> [-16 16)
 * Round Mode          : convergent
 * Overflow Mode       : wrap
 */


#define NL  3
const int16_t B[3] = {
     1600,   3199,   1600
};
#define DL 3
const int16_t A[3] = {
    16384, -15447,   5461
};
