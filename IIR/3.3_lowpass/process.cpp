#include <stdint.h>
#include <process.h>
#include "IIR_float.h"
#include <math.h>

// this program can only run by using Qt creator, the goal here is to make an IIR lowpass 1Khz 2orde
// implementation of an IIR DF-1 OR DF-2 using type double coefficient
// the program can be configured to use in a Microcontroller if floating point is supported
// note that for higer orde the code must be change to second orde section which is not coverd here
// https://en.wikipedia.org/wiki/Digital_biquad_filter
static const int N = NL;
static double input[N]  = {0};
static double output[N] = {0};


int16_t process_left_sample ( int16_t sample )
{

////////////////////////////// Direct 1 single section

        input[0] = sample;
        double temp=0;
        for(int i=0; i <=N-1;i++)
        {
            temp = temp + (((B[i])*input[i])); // sum up the b coefficient
        }
        //output[0] = temp;
        for(int i=1; i <= N-1;i++)
        {
            temp = temp - ((A[i]*output[i]));// subtract the a coefficient
        }
        output[0]= temp; //output/A0
        for(int i =N-1; i > 0;i--)
        {
            input[i] =  input[i-1]; // shift delay term for next iteration x[n-012]
            output[i] =  output[i-1];
        }
        return output[0];
}

int16_t process_right_sample ( int16_t sample )
  {
    ///////////////////////////// Direct 2 single section
   //    Wn_0[0] = sample;
   //    double Yn=0;
   //    for(int i=1; i <=N-1;i++)
   //    {
   //        Wn_0[0] = Wn_0[0] - (A[i]*Wn_0[i]); // Wn = -Ak*Xn
   //    }
   //    //output[0] = temp;
   //    for(int i=0; i <= N-1;i++)
   //    {
   //        Yn = Yn + (B[i] * Wn_0[i]); // Bk * Wn = Yn
   //    }
   //    for(int i =N-1; i > 0;i--)
   //    {
   //        Wn_0[i] = Wn_0[i-1]; // shift delay
   //    }
   //    return Yn;
    return sample;
  }
