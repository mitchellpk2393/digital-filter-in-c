#ifndef HR_BROJZ_TEMPERATUURWINDOW_H
#define HR_BROJZ_TEMPERATUURWINDOW_H

#include <QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QTimer>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>
#include <QSpinBox>
#include <QSlider>
#include <QWidget>
#include <QDateTime>
#include <QFont>
#include <fstream>
#include <iomanip>

QT_CHARTS_USE_NAMESPACE

class ControlWindow: public QMainWindow {
    Q_OBJECT
public:
    explicit ControlWindow(QWidget *parent = nullptr);
    virtual ~ControlWindow();
    void setX(double value);
    double getY();
    int getSetpoint();
    void setTickInterval(int value);
private:
    QChartView* chartView;
    QChart* chart;
    QLineSeries* seriesX;
    QLineSeries* seriesY;
    QLineSeries* seriesS;
    QTimer* timer;
    QHBoxLayout* layoutH;
    QVBoxLayout* layoutV;
    QLabel* label1;
    QLabel* label2;
    QPushButton* startStop;
    QCheckBox* checkboxBediening;
    QCheckBox* checkboxRuis;
    QSpinBox* spinbox;
    QFrame* line;
    QSlider* slider;
    QWidget* widget;
    std::ofstream ofile;

    const int maxSamples {4000};
    int setpoint {60};

    double x {0};
    double y {0};

    bool running {false};
    int tickInterval {1};

    void tick();
    void inputChanged(int value);
    void bedieningChanged(int value);
    void ruisChanged(int value);
    void setpointChanged(int value);
    void startStopClicked();
};

extern bool addNoise;

#endif // HR_BROJZ_TEMPERATUURWINDOW_H
