#include "regelaar.h"
#include "math.h"
void init(void) {
    setIntervalTimer(5);
}
/*this is a open loop method approach
where the systeem constant or determine by an 1ste orde systeem analyse
The step response of a pure first order system in percent of the change of the output signal
is equal to (1 − e^(-t/tsys)·100%)
If we enter t = τsys this gives about 63.2% and if we add t =1/3*tsys give us a value of 28.3%
further more the systeem constant where determine by a setting a stap of 100% which = PWM(1000)
*/
// pre define
double q0 =0,q1= 0,r0 = 0,r1=0,r2=0,r3=0;
double td = 0,ts= 0.5 ,ti = 0, kc =0;
double ksys =0.63*100,tsys= 27.15 ,teta = 3.45; //kc *100 for the correct unit
double output[4] = {0};
double error[2] = {0};
double setpoint = 0;
double input[2] = {0};
double pid = 0;
double aGAIN = 0.9; // the gain of the LPF filter
double check = 0;
int j =1; // help variabels
int k =3;

void regel(void)
{
    //PID parameters minimum-ITAE
    kc =  ((1.357/ksys)*(pow((teta / tsys),-0.947)));
    ti = ((tsys/0.842)*(pow((teta / tsys),0.738)));
    td = (0.382*tsys*(pow((teta / tsys),0.995)));
    //FRM+3points formula coefficient
    // r term = y-term which indicate the ouput
    // for PID Type B, the D term is subsisitue with y-term so it doesnt react aggressive when there is various in de setpoint
    q0 = kc;
    q1 = kc * (-1 + (ts/ti));
    r0 = kc * ((-3*td /2*ts));
    r1 = kc * ((7*td / 2*ts));
    r2 = kc * ((-5*td /2*ts));
    r3 = kc * ((td/2*ts));
    //
    // 1ste orde LPF for ADC measurment, not needed when no noise is applied
    input[0] = (aGAIN*input[1])+(readADC()/4.0)-(aGAIN*(readADC()/4.0)); // adc to degree C
    input[1]= input[0];
    //
    setpoint = (double)(getSetpoint());
    // shifting the delay output value
    output[k-3] = output[k-2];
    output[k-2] = output[k-1];
    output[k-1] = output[k];
    output[k] = input[0];  // input is not the input of the PID but the error terms + output terms hence PID type B
    for(int i = 0; i <= 1;i++){
        error[i] = (setpoint - output[j-i]);
    }
    //E.q of PID tpye B using veloctiy algoritme
    pid = pid + (q0 * error[0]) + (q1 * error[1]) + (r0 * output[0])+
    (r1 * output[1])+(r2 * output[2])+(r3 * output[3]);
    // saturating the PID ouput to 0-100%
    if (pid < 0){
        setPWM(0);
    }
    else if(pid > 100){
        setPWM(1000);
    }
    else{
       setPWM(pid*10); // adjusting the pwm for the process(plant)
    }

}
